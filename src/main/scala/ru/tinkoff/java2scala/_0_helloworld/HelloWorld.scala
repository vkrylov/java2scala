package ru.tinkoff.java2scala._0_helloworld

object HelloWorld {
  def howToWriteHelloWorldInScala: String =
    """|object Main extends App {
       |  println("Hello world!")
       |}
       |""".stripMargin
}


object HelloWorldApp extends App {
  println(HelloWorld.howToWriteHelloWorldInScala)
}